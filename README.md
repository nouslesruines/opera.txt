﻿# opera.txt

*Apophtegmes pour un numérique des ordures*

Version tmp / Sur le métier / En parallèle de [l'écriture d'une ville](http://www.tierslivre.net/revue/spip.php?article217)

Pour sa version dynamique : https://ruines.org/opera.txt/

Catégories (en cours) : 1) L’ontologie écrivante 2) outillages chiffrés 3) imaginaire 4) écrire en tant que pluralité 5) écrire en tant que révolte 6) les marges holographiques 7) une grammaire mensonge 8) matérialisme et beauté 9) écriture et biologie
